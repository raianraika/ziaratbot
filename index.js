/**
 * Created by apple on 8/11/15 AD.
 */


/*
 db.push("/" + userInfo.info.id, {info: userInfo.info, state: userInfo.state, type:userInfo.type, phone: userInfo.phone});
 */
var Bot = require("./lib/Bot");
var fs = require("fs");
var JsonDB = require('node-json-db');
var sReq = require("sync-request");
var db = new JsonDB("phoneToIdDb", true, false);
var config = require("./configLoader");
var i18n = require("i18next");
i18n.init({
    saveMissing: true,
    debug: false

});
function defaultKeyboardForUserType(userInfo) {
    console.log("submitMode="+userInfo.submitMode);
    if (userInfo.submitMode == 0) {
        if (userInfo.type == 2)
            return [
                [i18n.t("menu.main.seeDellAndBeNaieb")],
                [i18n.t("menu.main.seePhoto"), i18n.t("menu.main.salavat"), i18n.t("menu.main.setName")]
            ]
        else // show taleb menu
        {
            return [
                [i18n.t("menu.del.make")],
                [i18n.t("menu.main.seePhoto"), i18n.t("menu.main.salavat"), i18n.t("menu.main.setName"), i18n.t("menu.main.seeDell")]
            ]
        }
    } else {
        if (userInfo.type == 2)
            return [
                [i18n.t("menu.main.sendApp"), i18n.t("menu.main.setName")],
                [i18n.t("menu.main.seePhoto"), i18n.t("menu.main.salavat")]
            ]
        else // show taleb menu
        {
            return [
                [i18n.t("menu.main.sendApp"), i18n.t("menu.main.setName")],
                [i18n.t("menu.main.seePhoto"), i18n.t("menu.main.salavat")]
            ]
        }
    }
}
function defaultAnswerText(userInfo) {
    if (userInfo.submitMode == 0) {
        if (userInfo.type == 2)// show naieb menu
        {
            return i18n.t("menu.main.whatDoNext?_Nayeb");
        }
        else // show taleb menu
        {
            return i18n.t("menu.main.whatDoNext?_Taleb")
        }
    } else {
        if (userInfo.type == 2)// show naieb menu
        {
            return i18n.t("menu.main.whatDoNext?_Nayeb_submitMode");
        }
        else // show taleb menu
        {
            return i18n.t("menu.main.whatDoNext?_Taleb_submitMode")
        }
    }
}

var bot = new Bot({
    token: config.getToken()
})
    .on('message', function (msg) {

        console.log(msg);
        if (msg.text == undefined) {
            return;
        }
        // check user is new or old

        var userInfo = false;
        try {
            userInfo = db.getData("/" + msg.from.id);
        }
        catch (err) {
            console.log("user not found");
        }

        if (userInfo == false) {
            // first add user to db
            try {
                db.push("/" + msg.from.id, {submitMode: 0, info: msg.from, state: -1, type: -1, phone: -1});
                bot.sendMessage({
                    chat_id: msg.from.id,
                    text: "کمپین نائب‌الزیاره برنامه‌ای فرهنگی و مذهبی است که در آن شهروندان مشهدی و زائران بارگاه ملکوتی علی بن موسی الرضا(ع) برای آن دسته از افرادی نایب می‌شوند که به هر دلیل نمی‌توانند به زیارت این امام رئوف نائل شوند."
                });
                bot.sendMessage({
                    chat_id: msg.from.id,
                    text: i18n.t("menu.reg.letMeKnowYourPhone"),
                    reply_markup: {
                        hide_keyboard: true}
                })

            }
            catch (err) {
                console.log("err" + err);
            }

            // this is a new user so show reg menu


        }

        else {
            console.log("there is such user:\n" + userInfo.info + "\n" + userInfo.state + "\n" + userInfo.type);
            // check if user set phone ?!
            if (userInfo.phone == -1) {
                command = msg.text.replace("/", "");
                if (/^09\d{9}$/.test(command) || /^9\d{9}$/.test(command) || /^0098\d{9}$/.test(command)) {
                    db.push("/" + userInfo.info.id, {submitMode: userInfo.submitMode, info: userInfo.info, state: userInfo.state, type: userInfo.type, phone: command});
                    bot.sendMessage({
                        chat_id: msg.from.id,
                        text: i18n.t("menu.reg.naiebOrTaleb"),
                        reply_markup: {
                            keyboard: [
                                [i18n.t("menu.reg.Naieb"), i18n.t("menu.reg.Taleb")]
                            ],
                            one_time_keyboard: true
                        }
                    });
                    return;
                }
                else {
                    // show message invalid phone number
                    bot.sendMessage({
                        chat_id: msg.from.id,
                        text: i18n.t("menu.reg.letMeKnowYourPhoneInvalidTryAgain"),
                        reply_markup: {
                            hide_keyboard: true}
                    });
                    return;


                }
            }

            // check if user set type ?!
            if (userInfo.type == -1) {
                console.log("are u kidiing ?!");
                cmd = msg.text.replace("/", "");

                switch (cmd) {
                    case i18n.t("menu.reg.Naieb"):

                        var resp = sReq('GET', encodeURI(config.getBaseUrl() + "type=register&name=" + userInfo.info.first_name + "&mobile=" + userInfo.phone + "&userType=" + 2 + "&telId=" + msg.from.id));

                        if (resp.statusCode == 200) {
                            resp = JSON.parse(resp.body.toString("utf8"));
                            console.log("resp : " + JSON.stringify(resp));
                            console.log("user Type : " + userInfo.type);
                            if (resp.result) {

                                db.push("/" + userInfo.info.id, {submitMode: userInfo.submitMode, info: userInfo.info, state: userInfo.state, type: Number(resp.type), phone: userInfo.phone});// 1 is Taleb
                                if ( userInfo.type != Number(resp.type)) {
                                    bot.sendMessage({
                                        chat_id: msg.from.id,
                                        text: i18n.t("menu.main.youAreLoginLastAS") + " " + (Number(resp.type) == 1 ? i18n.t("menu.reg.Taleb") : i18n.t("menu.reg.Naieb")) + i18n.t("menu.main.youAreLoginLastTaile")
                                    });
                                }

                                userInfo.type = Number(resp.type);


                                // show main menu
                                key = defaultKeyboardForUserType(userInfo);
                                var answerText=defaultAnswerText(userInfo);
                                bot.sendMessage({
                                    chat_id: msg.from.id,
                                    text: answerText,
                                    reply_markup: {
                                        keyboard: key,
                                        one_time_keyboard: true
                                    }
                                });
                            }

                        }


                        break;
                    case i18n.t("menu.reg.Taleb"):

                        var resp = sReq('GET', encodeURI(config.getBaseUrl() + "type=register&name=" + userInfo.info.first_name + "&mobile=" + userInfo.phone + "&userType=" + 1 + "&telId=" + msg.from.id));


                        if (resp.statusCode == 200) {
                            resp = JSON.parse(resp.body.toString("utf8"));

                            if (resp.result) {
                                db.push("/" + userInfo.info.id, {submitMode: userInfo.submitMode, info: userInfo.info, state: userInfo.state, type: Number(resp.type), phone: userInfo.phone});// 1 is Taleb
                                // show main menu
                                /*bot.sendMessage({
                                 chat_id: msg.from.id,
                                 text: i18n.t("menu.main.whatDoNext?"),
                                 reply_markup: {
                                 keyboard: [
                                 [i18n.t("menu.main.setName"), i18n.t("menu.main.seeDell")],
                                 [i18n.t("menu.main.seePhoto"), i18n.t("menu.del.make")]
                                 ],
                                 one_time_keyboard: true
                                 }
                                 });*/
                                if ( userInfo.type != Number(resp.type)) {
                                    bot.sendMessage({
                                        chat_id: msg.from.id,
                                        text: i18n.t("menu.main.youAreLoginLastAS") + " " + (Number(resp.type) == 1 ? i18n.t("menu.reg.Taleb") : i18n.t("menu.reg.Naieb")) + i18n.t("menu.main.youAreLoginLastTaile")
                                    });
                                }

                                userInfo.type = Number(resp.type);
                                key = defaultKeyboardForUserType(userInfo);
                                var answerText=defaultAnswerText(userInfo);
                                bot.sendMessage({
                                    chat_id: msg.from.id,
                                    text: answerText,
                                    reply_markup: {
                                        keyboard: key,
                                        one_time_keyboard: true
                                    }
                                });
                            }

                        }

                        break;
                    default :
                        bot.sendMessage({
                            chat_id: msg.from.id,
                            text: i18n.t("menu.reg.naiebOrTaleb"),
                            reply_markup: {
                                keyboard: [
                                    [i18n.t("menu.reg.Naieb"), i18n.t("menu.reg.Taleb")]
                                ],
                                one_time_keyboard: true
                            }
                        });
                        break;
                }
                return;

            }
            // user set information so show main menu
            else {

                if (userInfo.state != -1) {
                    // its in command mode
                    switch (userInfo.state) {
                        case 1:

                            // it's in set name mode
                            var name = msg.text;
                            if (name != "" && name != undefined) {
                                //db.push("/" + userInfo.info.id, {info: userInfo.info, state: 1, type: userInfo.type, phone: userInfo.phone});
                                var resp = sReq('GET', encodeURI(config.getBaseUrl() + "type=updateName&name=" + name + "&mobile=" + userInfo.phone));
                                if (resp.statusCode == 200) {
                                    resp = JSON.parse(resp.body.toString("utf8"));

                                    if (resp.result) {

                                        userInfo.info.first_name = name;
                                        db.push("/" + userInfo.info.id, {submitMode: userInfo.submitMode, info: userInfo.info, state: -1, type: userInfo.type, phone: userInfo.phone});
                                        bot.sendMessage({
                                            chat_id: msg.from.id,
                                            text: name + ", " + i18n.t("menu.setName.YourNameChanged")
                                        });
                                        key = defaultKeyboardForUserType(userInfo);
                                        var answerText=defaultAnswerText(userInfo);
                                        bot.sendMessage({
                                            chat_id: msg.from.id,
                                            text: answerText,
                                            reply_markup: {
                                                keyboard: key,
                                                one_time_keyboard: true
                                            }
                                        });

                                    }
                                    else {
                                        bot.sendMessage({
                                            chat_id: msg.from.id,
                                            text: i18n.t("err.userNameNotSetTryAgain")
                                        })
                                        return;
                                    }

                                }


                            }

                            break;
                        // end of set name command
                        // see del
                        case 2:
                            console.log("salam ");
                            var cmd = msg.text.replace("/", "");
                            switch (cmd) {
                                case i18n.t("menu.main.back"):
                                    db.push("/" + userInfo.info.id, {submitMode: userInfo.submitMode, info: userInfo.info, state: -1, type: userInfo.type, phone: userInfo.phone});
                                    key = defaultKeyboardForUserType(userInfo);
                                    var answerText=defaultAnswerText(userInfo);
                                    bot.sendMessage({
                                        chat_id: msg.from.id,
                                        text: answerText,
                                        reply_markup: {
                                            keyboard: key,
                                            one_time_keyboard: true
                                        }
                                    });
                                    break;
                                case i18n.t("menu.del.next"):
                                    var resp = sReq("GET", encodeURI(config.getBaseUrl() + "type=randQuote"));

                                    if (resp.statusCode == 200) {
                                        var info = JSON.parse(resp.body.toString("utf8"));
                                        if (info.result == true) {
                                            if (info.info.length == 1) {
                                                info = info.info[0];
                                                var del = info.text;
                                                var postid = info.PostId;
                                                db.push("/" + userInfo.info.id, {submitMode: userInfo.submitMode, info: userInfo.info, state: 2, type: userInfo.type, phone: userInfo.phone, quoteID: postid});
                                                // if user is an naieb can replay

                                                if (userInfo.type == 2) {
                                                    key = [
                                                        [i18n.t("menu.del.replay")],
                                                        [i18n.t("menu.main.back"), i18n.t("menu.del.next")]

                                                    ]
                                                }
                                                else {
                                                    key = [
                                                        [i18n.t("menu.del.make")],
                                                        [i18n.t("menu.main.back"), i18n.t("menu.del.next")]

                                                    ]
                                                }

                                                bot.sendMessage({
                                                    chat_id: msg.from.id,
                                                    text: del,
                                                    reply_markup: {
                                                        keyboard: key
                                                    }
                                                })
                                            }
                                        }
                                    }
                                    console.log("we are in end of case 2 ");
                                    break;
                                case i18n.t("menu.del.replay"):
                                    console.log(cmd);
                                    console.log("we are in " + i18n.t("menu.del.replay"))
                                    if (userInfo.type == 2) {
                                        db.push("/" + userInfo.info.id, {submitMode: userInfo.submitMode, info: userInfo.info, state: 3, type: userInfo.type, phone: userInfo.phone, quoteID: userInfo.quoteID});
                                        bot.sendMessage({
                                            chat_id: msg.from.id,
                                            text: i18n.t("main.sendDell.replayPlzWriteYours"),
                                            reply_markup: {
                                                hide_keyboard: true
                                            }
                                        })

                                    }
                                    else {
                                        if (userInfo.type == 2) {
                                            key = [
                                                [i18n.t("menu.del.replay")],
                                                [i18n.t("menu.main.back"), i18n.t("menu.del.next")]

                                            ]
                                        }
                                        else {
                                            key = [
                                                [i18n.t("menu.del.make")],
                                                [i18n.t("menu.main.back"), i18n.t("menu.del.next")]

                                            ]
                                        }

                                        bot.sendMessage({
                                            chat_id: msg.from.id,
                                            text: i18n.t("err.youCanNotMakeReplayYouAreTaleban"),
                                            reply_markup: {
                                                keyboard: key
                                            }
                                        })
                                        return;
                                    }

                                    break;
                                case i18n.t("menu.del.make"):
                                    console.log("it' a make del request");
                                    // if user is an taleb ( code type 1 )
                                    if (userInfo.type == 1) {

                                        // change state to 4 and get del

                                        db.push("/" + userInfo.info.id, {submitMode: userInfo.submitMode, info: userInfo.info, state: 4, type: userInfo.type, phone: userInfo.phone, quoteID: userInfo.quoteID});
                                        // send msg to user
                                        bot.sendMessage({
                                            chat_id: msg.from.id,
                                            text: i18n.t("menu.del.pleasLetMeKnowYourQoute"),
                                            reply_markup: {
                                                hide_keyboard: true
                                            }
                                        });


                                    }
                                    else {

                                        if (userInfo.type == 2) {
                                            key = [
                                                [i18n.t("menu.del.replay")],
                                                [i18n.t("menu.main.back"), i18n.t("menu.del.next")]

                                            ]
                                        }
                                        else {
                                            key = [
                                                [i18n.t("menu.del.make")],
                                                [i18n.t("menu.main.back"), i18n.t("menu.del.next")]

                                            ]
                                        }

                                        bot.sendMessage({
                                            chat_id: msg.from.id,
                                            text: i18n.t("err.youCanNotMakeDelYouAreNaieb"),
                                            reply_markup: {
                                                keyboard: key
                                            }
                                        })
                                        return;
                                    }
                                    break;
                                default :
                                    if (userInfo.type == 2) {
                                        key = [
                                            [i18n.t("menu.del.replay")],
                                            [i18n.t("menu.main.back"), i18n.t("menu.del.next")]

                                        ]
                                    }
                                    else {
                                        key = [
                                            [i18n.t("menu.del.make")],
                                            [i18n.t("menu.main.back"), i18n.t("menu.del.next")]

                                        ]
                                    }

                                    bot.sendMessage({
                                        chat_id: msg.from.id,
                                        text: i18n.t("err.invalidInputForDel"),
                                        reply_markup: {
                                            keyboard: key
                                        }
                                    })
                                    break;
                                // end of brows del
                                // make del replay
                            }
                            break;
                        case 3:
                            console.log("in replay mode ");
                            db.push("/" + userInfo.info.id, {submitMode: userInfo.submitMode, info: userInfo.info, state: 2, type: userInfo.type, phone: userInfo.phone, quoteID: userInfo.quoteID});

                            var repl = msg.text;
                            if (repl != '') {

                                //if (userInfo.type == 2) {
                                //    key = [[i18n.t("menu.del.replay")],
                                //        [i18n.t("menu.main.back"), i18n.t("menu.del.next")]
                                //
                                //    ]
                                //}
                                //else {
                                //    key = [[i18n.t("menu.del.make")],
                                //        [i18n.t("menu.main.back"), i18n.t("menu.del.next")]
                                //
                                //    ]
                                //}
                                var resp = sReq("GET", encodeURI(config.getBaseUrl() + "type=reply&mobile=" + userInfo.phone + "&text=" + repl + "&quoteId=" + userInfo.quoteID));
                                if (resp.statusCode == 200) {

                                    resp = JSON.parse(resp.body.toString("utf8"));

                                    if (resp.result) {
                                        db.push("/" + userInfo.info.id, {submitMode: 1, info: userInfo.info, state: -1, type: userInfo.type, phone: userInfo.phone, quoteID: userInfo.quoteID});
                                        userInfo.submitMode = 1;
                                        key = defaultKeyboardForUserType(userInfo);
                                        bot.sendMessage({
                                            chat_id: msg.from.id,
                                            text: i18n.t("menu.del.yourDelAccepted"),
                                            reply_markup: {
                                                keyboard: key,
                                                one_time_keyboard: true
                                            }

                                        })
                                    }
                                    else {
                                        bot.sendMessage({
                                            chat_id: msg.from.id,
                                            text: resp.msg,
                                            reply_markup: {
                                                keyboard: key,
                                                one_time_keyboard: true
                                            }

                                        })
                                    }
                                }
                                else {
                                    console.log("we have an err in send replay ")
                                    // todo send err to user and tell him/her retry again latter !
                                }
                            }
                            break;
                        case 4:
                            var mText = msg.text;
                            if (mText != null || mText != undefined) {
                                console.log("mtext " + mText);
                                db.push("/" + userInfo.info.id, {submitMode: userInfo.submitMode, info: userInfo.info, state: 2, type: userInfo.type, phone: userInfo.phone, quoteID: userInfo.quoteID});
                                // so let me send del to server
                                var resp = sReq("GET", encodeURI(config.getBaseUrl() + "type=setInfo&mobile=" + userInfo.phone + "&text=" + mText + "&typeId=1"));

                                if (resp.statusCode == 200) {
                                    resp = JSON.parse(resp.body.toString("UTF8"));
                                    if (resp.result == true) {
                                        db.push("/" + userInfo.info.id, {submitMode: 1, info: userInfo.info, state: -1, type: userInfo.type, phone: userInfo.phone});
                                        userInfo.submitMode = 1;
                                        key = defaultKeyboardForUserType(userInfo);
                                        bot.sendMessage({
                                            chat_id: msg.from.id,
                                            text: i18n.t("dell.yourDellAccpted"),
                                            reply_markup: {
                                                keyboard: key,
                                                one_time_keyboard: true
                                            }
                                        })
                                        return;
                                    }
                                    else {
                                        //TODO if system not response send msg to user
                                        if (userInfo.type == 2) {
                                            key = [
                                                [i18n.t("menu.del.replay")],
                                                [i18n.t("menu.main.back"), i18n.t("menu.del.next")]

                                            ]
                                        }
                                        else {
                                            key = [
                                                [i18n.t("menu.del.make")],
                                                [i18n.t("menu.main.back"), i18n.t("menu.del.next")]

                                            ]
                                        }

                                        bot.sendMessage({
                                            chat_id: msg.from.id,
                                            text: i18n.t("err.yourDelNotAcceptedServerProblem"),
                                            reply_markup: {
                                                keyboard: key
                                            }
                                        })
                                        return;
                                    }
                                }
                                else {
                                    // this is error in send response
                                    if (userInfo.type == 2) {
                                        key = [
                                            [i18n.t("menu.del.replay")],
                                            [i18n.t("menu.main.back"), i18n.t("menu.del.next")]

                                        ]
                                    }
                                    else {
                                        key = [
                                            [i18n.t("menu.del.make")],
                                            [i18n.t("menu.main.back"), i18n.t("menu.del.next")]

                                        ]
                                    }

                                    bot.sendMessage({
                                        chat_id: msg.from.id,
                                        text: i18n.t("err.serverErr"),
                                        reply_markup: {
                                            keyboard: key
                                        }
                                    })
                                    return;
                                }

                            }
                            else {
                                if (userInfo.type == 2) {
                                    key = [
                                        [i18n.t("menu.del.replay")],
                                        [i18n.t("menu.main.back"), i18n.t("menu.del.next")]

                                    ]
                                }
                                else {
                                    key = [
                                        [i18n.t("menu.del.make")],
                                        [i18n.t("menu.main.back"), i18n.t("menu.del.next")]

                                    ]
                                }

                                bot.sendMessage({
                                    chat_id: msg.from.id,
                                    text: i18n.t("err.yourTextIsEmptyTryAgain"),
                                    reply_markup: {
                                        keyboard: key
                                    }
                                })
                                return;
                            }
                            break;
                        case 5:
                            var cmd = msg.text.replace("/", "");
                            switch (cmd) {
                                case i18n.t("menu.main.back"):
                                    db.push("/" + userInfo.info.id, {submitMode: userInfo.submitMode, info: userInfo.info, state: -1, type: userInfo.type, phone: userInfo.phone});
                                    key = defaultKeyboardForUserType(userInfo);
                                    var answerText=defaultAnswerText(userInfo);
                                    bot.sendMessage({
                                        chat_id: msg.from.id,
                                        text: answerText,
                                        reply_markup: {
                                            keyboard: key,
                                            one_time_keyboard: true
                                        }
                                    });
                                    break;
                                case i18n.t("menu.pic.next"):

                                    console.log("in pic next ");
                                    var resp = sReq("GET", encodeURI(config.getBaseUrl() + "type=randPic&mobile=" + userInfo.phone));
                                    if (resp.statusCode == 200) {

                                        resp = JSON.parse(resp.body.toString("utf8"));
                                        if (resp.result == true) {


                                            imgPath = config.getBasePath() + resp.info[0].image;
                                            db.push("/" + userInfo.info.id, {submitMode: userInfo.submitMode, info: userInfo.info, state: userInfo.state, type: userInfo.type, phone: userInfo.phone, photoID: resp.info[0].PostId, likeNumber: resp.info[0].likeNumber, isLike: resp.info[0].islike});
                                            if (resp.info[0].islike == 1) {
                                                key = [
                                                    [i18n.t("menu.main.back"), i18n.t("menu.pic.next")],
                                                    [i18n.t("menu.pic.Like")]

                                                ]
                                            }
                                            else {
                                                key = [
                                                    [i18n.t("menu.main.back"), i18n.t("menu.pic.next")],
                                                    [i18n.t("menu.pic.Like")]
                                                ]
                                            }
                                            bot.sendPhoto({
                                                chat_id: msg.from.id,
                                                caption: (resp.info[0].islike == 1 ? i18n.t("menu.pic.youLikeThisPhotoAnd") : "") + "  " + resp.info[0].likeNumber + "  " + i18n.t("menu.pic.likedByOtherPepole"),
                                                files: {
                                                    photo: imgPath
                                                },
                                                reply_markup: {
                                                    keyboard: key,
                                                    one_time_keyboard: true
                                                }
                                            })
                                        }
                                        else {
                                            key = [
                                                [i18n.t("menu.main.back"), i18n.t("menu.pic.next")],
                                                [i18n.t("menu.pic.Like")]

                                            ];
                                            bot.sendMessage({
                                                chat_id: msg.from.id,
                                                text: resp.msg,
                                                reply_markup: {
                                                    keyboard: key
                                                }
                                            })
                                        }

                                    }
                                    else {
                                        key = [
                                            [i18n.t("menu.main.back"), i18n.t("menu.pic.next")],
                                            [i18n.t("menu.pic.Like")]

                                        ];
                                        bot.sendMessage({
                                            chat_id: msg.from.id,
                                            text: i18n.t("err.serverNotResponse"),
                                            reply_markup: {
                                                keyboard: key
                                            }
                                        })
                                    }

                                    break;
                                case i18n.t("menu.pic.disLike"):
                                    // dislike
                                    if (userInfo.isLike == 1 && userInfo.photoID != undefined) {

                                        var resp = sReq("GET", encodeURI(config.getBaseUrl() + "type=like&mobile=" + userInfo.phone + "&postId=" + userInfo.photoID + "&typeId=4"));
                                        if (resp.statusCode == 200) {

                                            resp = JSON.parse(resp.body.toString("utf8"));
                                            if (resp.result == true) {

                                                bot.sendMessage({
                                                    chat_id: msg.from.id,
                                                    text: i18n.t("main.pic.youDisLikeThisPhoto")
                                                })
                                            }
                                            else {
                                                // there is some thing wrong?!
                                                bot.sendMessage({
                                                    chat_id: msg.from.id,
                                                    text: resp.msg
                                                });
                                            }
                                        }
                                        else {
                                            // todo server not response
                                            bot.sendMessage({
                                                chat_id: msg.from.id,
                                                text: i18n.t("err.pic.serverNotAnswer")
                                            })
                                        }

                                    }

                                    break;
                                case i18n.t("menu.pic.Like"):
                                    // like photo

                                    if (userInfo.isLike == 0 && userInfo.photoID != undefined) {

                                        var resp = sReq("GET", encodeURI(config.getBaseUrl() + "type=like&mobile=" + userInfo.phone + "&postId=" + userInfo.photoID + "&typeId=4"));
                                        if (resp.statusCode == 200) {

                                            resp = JSON.parse(resp.body.toString("utf8"));
                                            if (resp.result == true) {

                                                bot.sendMessage({
                                                    chat_id: msg.from.id,
                                                    text: i18n.t("main.pic.youLikeThisPhoto")
                                                })
                                            }
                                            else {
                                                // there is some thing wrong?!
                                                bot.sendMessage({
                                                    chat_id: msg.from.id,
                                                    text: resp.msg
                                                });
                                            }
                                        }
                                        else {
                                            // todo server not response
                                            bot.sendMessage({
                                                chat_id: msg.from.id,
                                                text: i18n.t("err.pic.serverNotAnswer")
                                            })
                                        }

                                    }

                                    break;
                                default:
                                    key = [
                                        [i18n.t("menu.main.back"), i18n.t("menu.pic.next")],
                                        [i18n.t("menu.pic.Like")]
                                    ];

                                    bot.sendMessage({
                                        chat_id: msg.from.id,
                                        text: i18n.t("menu.pic.defaultError"),
                                        reply_markup: {
                                            keyboard: key,
                                            one_time_keyboard: true
                                        }
                                    })
                                    break;
                            }

                            break;
                    }


                }
                else {

                    var cmd = msg.text.replace("/", "");
                    switch (cmd) {

                        case i18n.t("menu.main.setName"):
                            // set user name
                            db.push("/" + userInfo.info.id, {submitMode: userInfo.submitMode, info: userInfo.info, state: 1, type: userInfo.type, phone: userInfo.phone});
                            bot.sendMessage({
                                chat_id: msg.from.id,
                                text: i18n.t("menu.setName.whatSYourName"),
                                reply_markup: {
                                    hide_keyboard: true
                                }
                            });
                            break;
                        case i18n.t("menu.main.seeDellAndBeNaieb"):
                        case i18n.t("menu.main.seeDell"):
                            // see wall grand
                            // make a request to server for wall grand
                            var resp = sReq("GET", encodeURI(config.getBaseUrl() + "type=randQuote"));
                            if (resp.statusCode == 200) {
                                var info = JSON.parse(resp.body.toString("utf8"));
                                console.log("omiiiiiiiiiid         " + JSON.stringify(info));
                                if (info.result == true) {
                                    if (info.info.length == 1) {
                                        info = info.info[0];
                                        var del = info.text;
                                        var postid = info.PostId;
                                        db.push("/" + userInfo.info.id, {submitMode: userInfo.submitMode, info: userInfo.info, state: 2, type: userInfo.type, phone: userInfo.phone, quoteID: postid});
                                        // if user is an naieb can replay

                                        if (userInfo.type == 2) {
                                            key = [
                                                [i18n.t("menu.del.replay")],
                                                [i18n.t("menu.main.back"), i18n.t("menu.del.next")]

                                            ]
                                        }
                                        else {
                                            key = [
                                                [i18n.t("menu.del.make")],
                                                [i18n.t("menu.main.back"), i18n.t("menu.del.next")]

                                            ]
                                        }

                                        bot.sendMessage({
                                            chat_id: msg.from.id,
                                            text: del,
                                            reply_markup: {
                                                keyboard: key
                                            }
                                        })
                                    }
                                    else if (info.info.length == 0) {

                                        key = defaultKeyboardForUserType(userInfo);
                                        bot.sendMessage({
                                            chat_id: msg.from.id,
                                            text: i18n.t("menu.del.thereIsNoDelneveshteWithoutAnswer"),
                                            reply_markup: {
                                                keyboard: key
                                            }
                                        })
                                    }
                                }
                            }

                            break;
                        case i18n.t("menu.main.seePhoto"):
                            // see photo grand
                            // make request to wall grand

                            //      db.push("/" + userInfo.info.id, {info: userInfo.info, state: 5, type: userInfo.type, phone: userInfo.phone});
                            var resp = sReq("GET", encodeURI(config.getBaseUrl() + "type=randPic&mobile=" + userInfo.phone));
                            if (resp.statusCode == 200) {

                                resp = JSON.parse(resp.body.toString("utf8"));
                                if (resp.result == true) {
                                    imgPath = config.getBasePath() + resp.info[0].image;
                                    db.push("/" + userInfo.info.id, {submitMode: userInfo.submitMode, info: userInfo.info, state: 5, type: userInfo.type, phone: userInfo.phone, photoID: resp.info[0].PostId, likeNumber: resp.info[0].likeNumber, isLike: resp.info[0].islike});
                                    if (resp.info[0].islike == 1) {
                                        key = [
                                            [i18n.t("menu.main.back"), i18n.t("menu.pic.next")],
                                            [i18n.t("menu.pic.Like")]
                                        ]
                                    }
                                    else {
                                        key = [
                                            [i18n.t("menu.main.back"), i18n.t("menu.pic.next")],
                                            [ i18n.t("menu.pic.Like")]
                                        ]
                                    }
                                    bot.sendPhoto({
                                        chat_id: msg.from.id,
                                        caption: (resp.info[0].islike == 1 ? i18n.t("menu.pic.youLikeThisPhotoAnd") : "") + resp.info[0].likeNumber + " " + i18n.t("menu.pic.likedByOtherPepole"),
                                        files: {
                                            photo: imgPath
                                        },
                                        reply_markup: {
                                            keyboard: key,
                                            one_time_keyboard: true
                                        }
                                    });

                                }
                                else {
                                    key = [
                                        [i18n.t("menu.main.back"), i18n.t("menu.pic.next")],
                                        [ i18n.t("menu.pic.Like")]

                                    ];
                                    bot.sendMessage({
                                        chat_id: msg.from.id,
                                        text: resp.msg,
                                        reply_markup: {
                                            keyboard: key
                                        }
                                    })
                                }

                            }
                            else {
                                key = [
                                    [i18n.t("menu.main.back"), i18n.t("menu.pic.next")],
                                    [ i18n.t("menu.pic.Like")]

                                ];
                                bot.sendMessage({
                                    chat_id: msg.from.id,
                                    text: i18n.t("err.serverNotResponse"),
                                    reply_markup: {
                                        keyboard: key
                                    }
                                })
                            }

                            break;
                        case i18n.t("menu.del.make"):


                            // if user is an taleb ( code type 1 )
                            if (userInfo.type == 1) {

                                // change state to 4 and get del

                                db.push("/" + userInfo.info.id, {submitMode: userInfo.submitMode, info: userInfo.info, state: 4, type: userInfo.type, phone: userInfo.phone, quoteID: userInfo.quoteID});
                                // send msg to user
                                bot.sendMessage({
                                    chat_id: msg.from.id,
                                    text: i18n.t("menu.del.pleasLetMeKnowYourQoute"),
                                    reply_markup: {
                                        hide_keyboard: true
                                    }
                                });


                            }
                            else {

                                if (userInfo.type == 2) {
                                    key = [
                                        [i18n.t("menu.del.replay")],
                                        [i18n.t("menu.main.back"), i18n.t("menu.del.next")]

                                    ]
                                }
                                else {
                                    key = [
                                        [i18n.t("menu.del.make")]
                                            [i18n.t("menu.main.back"), i18n.t("menu.del.next")]
                                    ]
                                }

                                bot.sendMessage({
                                    chat_id: msg.from.id,
                                    text: i18n.t("err.youCanNotMakeDelYouAreNaieb"),
                                    reply_markup: {
                                        keyboard: key
                                    }
                                })
                                return;
                            }
                            break;
                        case i18n.t("menu.main.sendApp"):
                            bot.sendChatAction({
                                chat_id:msg.from.id,
                                action:"upload_document"
                            })
                            bot.sendDocument({
                                chat_id:msg.from.id,
                                caption:"ziarat.apk",
                                files:{
                                    filename:"ziarat.apk",
                                    contentType:'application/vnd.android.package-archive',
                                    stream: fs.createReadStream('./upload/ziarat.apk')
                                }
                            },function(err,mm){
                                key = defaultKeyboardForUserType(userInfo);
                                var answerText=defaultAnswerText(userInfo);
                                bot.sendMessage({
                                    chat_id: msg.from.id,
                                    text: answerText,
                                    reply_markup: {
                                        keyboard: key,
                                        one_time_keyboard: true
                                    }
                                });
                            });

                            break;
                        case i18n.t("menu.main.salavat"):
                            bot.sendDocument({
                                chat_id: msg.from.id,
                                caption: i18n.t("main.salavateKhaseTitle"),
                                files: {
                                    filename: 'Salavat.mp3',
                                    contentType: 'audio/mp3',
                                    stream: fs.createReadStream('./upload/ss.mp3')
                                }
                            }, function (err, msg) {
                                console.log(err);
                                console.log(msg);
                            });
                        // I cat break for this state ment to redirect after sending file to main menu

                        default :
                            key = defaultKeyboardForUserType(userInfo);
                            var answerText=defaultAnswerText(userInfo);
                            bot.sendMessage({
                                chat_id: msg.from.id,
                                text: answerText,
                                reply_markup: {
                                    keyboard: key,
                                    one_time_keyboard: true
                                }
                            });
                            break;

                    }
                }


            }


        }
    })
    .start();